# Strongs pour la BYM

## Etape 1 : codifier le texte

Pour afficher les strongs dans le texte, nous devons aboutir à une codification HTML basée sur la codification osis, surtout pour les attributs lemma et morph (https://wiki.crosswire.org/OSIS_Bibles#Marking_lemmas_.26_morphology, http://crosswire.org/osis/tutor.jsp, xsd: https://ebible.org/osisCore.2.1.1.xsd, http://www.crosswire.org/study/passagestudy.jsp?key=Genesis+1%3A1), robinson : http://www.modernliteralversion.org/bibles/bs2/RMAC/RMACindex.html,  Exemple :

`<verse osisID="Gen.1.1" sID="Gen.1.1"/><w lemma="strong:H07225">In the beginning</w> <w lemma="strong:H0430">God</w> <w morph="strongMorph:TH8804" lemma="strong:H0853 strong:H01254">created</w> <w lemma="strong:H08064">the heaven</w> <w lemma="strong:H0853">and</w> <w lemma="strong:H0776">the earth</w>.<verse eID="Gen.1.1"/>
<verse osisID="Gen.1.2" sID="Gen.1.2"/><w lemma="strong:H0776">And the earth</w> <w morph="strongMorph:TH8804" lemma="strong:H01961">was</w> <w lemma="strong:H08414">without form</w>, <w lemma="strong:H0922">and void</w>; <w lemma="strong:H02822">and darkness</w> <transChange type="added">was</transChange> <w lemma="strong:H06440">upon the face</w> <w lemma="strong:H08415">of the deep</w>. <w lemma="strong:H07307">And the Spirit</w> <w lemma="strong:H0430">of God</w> <w morph="strongMorph:TH8764" lemma="strong:H07363">moved</w> <w lemma="strong:H05921">upon</w> <w lemma="strong:H06440">the face</w> <w lemma="strong:H04325">of the waters</w>.<verse eID="Gen.1.2"/>`

- Pour le faire, il faut d'abord codifier le texte md de la BYM en s'appuyant sur le text des strongs KJV (https://gitlab.com/anjc/web/bjc-website/-/tree/master/content/texts/eng_kjv), exemple :
`
1:1	<w lemma="strong:H07225">Au commencement</w><!--Bereshit bara elohîm èth hashamaïm vehèth ah aretz. Dans ce passage le mot « èth » est composé de la première et la dernière lettre de l'alphabet hébraïque « aleph » et « tav ». Aleph Tav apparaît plus de 7 000 fois dans le Tanakh. Nous notons la présence d'Aleph Tav : au commencement (Ge. 1:1), avec la Lumière (Ge. 1:4), Adam (Ge. 4:1), Hanowk (Ge. 5:22), Yossef (Joseph, Ge. 37:23), Moshé (Moïse, Ex. 18:23), Aaron et le pectoral (Ex. 28:28-30), les Hébreux (No. 14:22), Shema Israël (De. 6:4). Aleph Tav est le symbole du caractère hébreu considéré comme la signature du Mashiah (Messie) : Et les Juifs regarderont vers Aleph Tav, lequel ils ont percé (Za. 12:10).--> <w lemma="strong:H0430">Elohîm</w> <w morph="strongMorph:TH8804" lemma="strong:H0853 strong:H01254">créa</w> <w lemma="strong:H08064">les cieux</w> <w lemma="strong:H0853">et</w> <w lemma="strong:H0776">la Terre</w>.
1:2	La Terre devint tohu et bohu<!--Les mots hébreux « tohuw » et « bohuw » désignent la confusion, le chaos, la vanité. Voir Jé. 4:23.-->. La ténèbre<!--Le mot hébreu est au singulier.--> était sur la face de l'abîme et l'Esprit d'Elohîm planait au-dessus des eaux.
`
> le tag <w *lemma*="strong:H07225"> ... </w> : encadre le mot qui correspond à une définition dans les strongs, H07225 est la définition dans le fichier des strongs. L'ajout de la classe: morph="strongMorph:TH8804" permet d'indiquer le type du mot

- définir une codification stong et morphologie pour la BYM en faisant la translation en commandes tex (customs) qui seront par la suite utilisés pour générer le html et le pdf pour la BYM. Exemple : https://gitlab.com/anjc/bjc-build/-/blob/master/bin/md2tex.py#L138 pour lemma et morph


> Cette première codification permettra d'avoir les strongs en anglais sur le texte de la BYM

## Etape 2 : traduire le strong anglais en français

- Méthode 1 : Se rendre sur le répertoire fr https://gitlab.com/anjc/bjc-strongs/-/tree/master/strongs et traduire tous les fichiers en français.
- Méthode 2 : crawler/récupérer un strong français existant (https://gitlab.com/anjc/bjc-strongs/-/blob/master/strongs/strongs.zip), faire la correspondance du résultat avec les strongs en anglais programmatiquement, le résultat donnera donc un strong directement en français, qu'il faudra enrichir avec des définitions plus juste et proche de la version KJ.

## Link to OSIS bibles :
    > https://github.com/bzerangue/osis-bibles
    > https://en.wikisource.org/wiki/Talk:Bible_(King_James)
    > https://crosswire.org/ftpmirror/pub/sword/experimentalraw/modules/genbook/rawgenbook/kjvgb/kjvgb.bdt
    > https://studybible.info/mac
    > 
