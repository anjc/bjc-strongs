#!/usr/bin/env python3

"""EN2FR

Translate KJV Strongs into multiple languages with DeepL API.
"""
import argparse
import json
import os
import sys

try:
    from requests import post

except ModuleNotFoundError as e:
    print("Some Modules are missing: {}".format(e))

# DeepL authorization key
DEEPL_AUTH_KEY = os.environ['DEEPL_AUTH_KEY']
# strong's keys to translate values
STRONG_KEYS_TO_TRANSLATE_VALUES = [
    "derivation",
    "kjv_def",
    "strongs_def",
    "outline"
]


def filter_files(file_extension, list_files):
    """Filter."""
    for file in list_files:
        if file.endswith(file_extension) and file[1].isdigit():
            yield file


def load(filename):
    """Load."""
    with open(filename, 'r', encoding='utf-8') as f:
        return json.load(f)


def error_msg(code):
    """Handle HTTP error codes."""
    return {
        400: "400: Bad request. Check error message and your parameters.",
        403: "403: Authorization failed. Check auth_key or account status.",
        404: "404: The requested resource could not be found.",
        413: "413: The request size exceeds the limit.",
        414: "414: URL request too long. Use a POST request.",
        429: "429: Too many requests. Please wait and resend your request.",
        456: "456: Quota exceeded. The character limit has been reached.",
        503: "503: Resource currently unavailable. Try again later.",
        502: "502 or 5**: Internal Server error"
    }.get(code, "Unknown error code")


def query(text, source_lang, target_lang):
    """Call DeepL API."""
    url = 'https://api.deepl.com/v2/translate'
    payload = {
        'auth_key': DEEPL_AUTH_KEY,
        'text': text,
        'source_lang': source_lang,
        'target_lang': target_lang,
        'preserve_formatting': 1,
        'tag_handling': 'xml'}
    q = post(url, data=payload)
    code = q.status_code
    if code != 200:
        sys.exit("\n\n>>> {}".format(error_msg(code)))
    return q.json()


def translate(strong_dict, From, To):
    """Translate."""
    tmp = {}
    # collecting the items to translate in a temp dictionary
    for key in STRONG_KEYS_TO_TRANSLATE_VALUES:
        val = strong_dict.get(key)
        if val is None:
            print(" >>> Missing key « {} » ".format(key))
            continue
        tmp[key] = val

    # updating the temp dictionary with the translated data
    for k, v in tmp.items():
        tmp[k] = query(v, From, To)['translations'][0]['text']

    # updating the main strong dictionary
    for k, v in tmp.items():
        strong_dict[k] = v

    return strong_dict


def save(data, filename):
    """Save."""
    with open(filename, 'w', encoding='utf-8') as fp:
        json.dump(data, fp, ensure_ascii=False, sort_keys=True, indent=4)


def main(src_path, dst_path, From='EN', To='FR'):
    """Translate the KJV strongs text."""
    try:
        # prevent execution of processed files after a failure
        items = set(os.listdir(src_path)) - set(os.listdir(dst_path))
        # translate the JSON files
        json_files = filter_files('.json', items)
        for i, json_file in enumerate(sorted(json_files)):
            print('{} - {}'.format(str(i + 1).rjust(5), json_file))
            # loading json file
            obj = load(
                os.path.normpath(os.path.join(src_path, json_file)))
            # translating text
            translated = translate(obj, From, To)
            # saving
            save(translated,
                 os.path.normpath(os.path.join(dst_path, json_file)))

    except FileNotFoundError as e:
        print("{}: « {} »".format(e.strerror, e.filename))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Translate KJV Strongs \
                                     into multiple languages with DeepL API.')
    parser.add_argument('-f', '--From', default='en',
                        help="Source language. Default: 'EN' (English)")
    parser.add_argument('-t', '--To', default='fr',
                        help="Target language. Default: 'FR' (French)")
    args = parser.parse_args()
    dirname = os.path.dirname(__file__)
    strong_dir = os.path.normpath(os.path.join(dirname, 'strongs'))
    src_dir = os.path.normpath(os.path.join(strong_dir, args.From))
    dst_dir = os.path.normpath(os.path.join(strong_dir, args.To))
    main(src_dir, dst_dir, args.From.upper(), args.To.upper())
