#!/usr/bin/env python3

import csv
import json
import os

dirname = os.path.dirname(__file__)
strongs_dir = dirname + '/strongs/fr/'

with open(dirname + '/strongs.csv', 'r', newline='') as csv_file:
    csv_data = csv.reader(csv_file, delimiter='|')

    for id, row in enumerate(csv_data):
        # get the keys from the first line
        if id == 0:
            keys = row
            continue

        # re-associate values with their keys
        strong = dict(zip(keys, row))

        strong_ref = strong.pop('strong')
        strong_filename = strongs_dir + strong_ref + '.json'

        with open(strong_filename, 'w') as strong_file:
            json.dump(strong, strong_file)
