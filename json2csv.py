#!/usr/bin/env python3

import json
import os

dirname = os.path.dirname(__file__)
json_dir = dirname + '/strongs/en/'

json_files = os.listdir(json_dir)
json_files = [f for f in json_files if f.endswith('.json')]
json_files.sort()

strong_keys = ('xlit', 'lemma', 'pron', 'derivation', 'frequency',
               'strongs_def', 'kjv_def', 'outline')

with open(dirname + '/strongs.csv', 'w') as csv_file:
    # write headline
    csv_file.write('strong|' + '|'.join(strong_keys) + '\n')

    for json_filename in json_files:
        with open(json_dir + json_filename, 'r') as json_file:
            data = json.load(json_file)

        # write current strong reference
        strong_ref = os.path.splitext(json_filename)[0]
        csv_file.write(strong_ref + '|')

        for key in strong_keys:
            # if key is defined in the current strong
            if key in data.keys():
                # write data and add delimiter '|' until it's the last item
                if strong_keys.index(key) != len(strong_keys) - 1:
                    csv_file.write(str(data[key]) + '|')
                else:
                    csv_file.write(str(data[key]))
            else:
                # empty
                csv_file.write('|')

        csv_file.write('\n')
